from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from app.resources import VacancyDetailsAdminResource
from .models import Category,Type,HashTag,District,Comfort,Requirement,Vacancy
# Register your models here.
class PersonDetailAdmin(ImportExportModelAdmin):
    list_display = ('id','title','content','address','salary','date','publish','post','category','type','district')
    list_filter = ['category', 'type','post','hash_tag','district','date']
    search_fields = ['title','sub_title','content','category', 'type','post','hash_tag','district','date']
    list_editable = ['post','publish','category']
    # ordering = ('field1', ...)
    resource_class = VacancyDetailsAdminResource
    
admin.site.register(Vacancy,PersonDetailAdmin)
admin.site.register(Category)
admin.site.register(Type)
admin.site.register(HashTag)
admin.site.register(District)
admin.site.register(Comfort)
admin.site.register(Requirement)