from django import forms
from .models import Vacancy 


# Create your forms here.
class VacanForm(forms.ModelForm):

    class Meta:
        model = Vacancy
        fields = ('image', 'title', 'sub_title', 'address', 'work_time', 'salary', 'phone', 'phone2', 'telegram', 'comfort', 'requirements', 'link', 'date', 'rate', 'category', 'type', 'hash_tag','district')
        