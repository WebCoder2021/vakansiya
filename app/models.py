from email.policy import default
from django.db import models

from users.models import CustomUser

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name
class Type(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name
class HashTag(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name
class District(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name
class Comfort(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name
class Requirement(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name
class Vacancy(models.Model):
    user = models.ForeignKey(CustomUser,on_delete=models.CASCADE)
    image = models.ImageField(upload_to='images/',blank=True,null=True,verbose_name="Rasm",default="vakansiya.png")
    title = models.CharField(max_length=250,verbose_name="Sarlavha")
    sub_title = models.CharField(max_length=500,blank=True,null=True,verbose_name="Qisqacha")
    content = models.TextField(blank=True,null=True,verbose_name="Mazmuni")
    address = models.CharField(max_length=255,blank=True,null=True,verbose_name="Manzil")
    work_time = models.CharField(max_length=255,blank=True,null=True,verbose_name="Ish vaqti")
    salary = models.CharField(max_length=25,blank=True,null=True,verbose_name="Maosh",default='Kelishilgan holda')
    phone = models.CharField(max_length=50,blank=True,null=True,verbose_name="Telefon raqam")
    phone2 = models.CharField(max_length=50,blank=True,null=True,verbose_name="Qo'shimcha telefon raqam")
    telegram = models.CharField(max_length=200,blank=True,null=True,verbose_name="Telegram manzil")
    comfort = models.ManyToManyField(Comfort,verbose_name="Qulayliklar",blank=True)
    requirements = models.ManyToManyField(Requirement,verbose_name="Majburiyatlar",blank=True)
    link = models.URLField(max_length=500,blank=True,null=True,verbose_name="Vakansiya uchun Link")
    date = models.DateField(blank=True,null=True,verbose_name="E'lon sanasi")
    publish = models.BooleanField(default=False,verbose_name="Yuborishga ruxsat")
    post = models.BooleanField(default=False,verbose_name="Jo'natilgan")
    category = models.ForeignKey(Category, on_delete=models.CASCADE,verbose_name="Kategoriyasi")
    type = models.ForeignKey(Type, on_delete=models.CASCADE,verbose_name="Turi")
    hash_tag = models.ManyToManyField(HashTag,verbose_name="Hash taglar")
    district = models.ForeignKey(District, on_delete=models.CASCADE,verbose_name="Tuman(shahar)")
    date_time = models.DateTimeField(auto_now_add=True)
    rate = models.CharField(max_length=200,blank=True,null=True,verbose_name="Ish stavkasi(h)")
    def __str__(self):
        return self.title