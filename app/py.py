# from tkinter import *
# import turtle
# turtle.bgcolor("light green")
# turtle.color("orange", "yellow")
# turtle.begin_fill()
# turtle.penup()
# turtle.goto(-250, 160)
# turtle.pendown()
# turtle.circle(45)
# turtle.end_fill()
 
# turtle.color("blue", "pink")
# turtle.begin_fill()
# turtle.penup()
# turtle.goto(85, 85)

# turtle.right(90) # qaytiw 90

# turtle.pendown()
# turtle.forward(120) 
# turtle.right(90)
# turtle.forward(120)
# turtle.right(90)
# turtle.forward(120)
 
# turtle.right(90)
# turtle.forward(120)
# turtle.end_fill()
 
# #tom
 
# turtle.color("blue", "brown")
# turtle.begin_fill()
# turtle.penup()
# turtle.goto(108, 85)
# turtle.right(225)
# turtle.pendown()
# turtle.forward(120)
 
# turtle.left(90)
# turtle.forward(120)
 
# turtle.left(135) # chapga 135
# turtle.forward(170)
# turtle.end_fill()
 
# # dereza
 
# turtle.color ("blue", "light blue")
# turtle.begin_fill()
# turtle.penup()
# turtle.goto(50, 50)
# turtle.right(90)
# turtle.pendown()
# turtle.forward(50)
# turtle.right(90)
# turtle.forward(50)
# turtle.right(90)
# turtle.forward(50)
# turtle.right(90)
# turtle.forward(50)
# turtle.end_fill()
 
# # mo'risi
 
# turtle.color("blue", "grey")
# turtle.begin_fill()
# turtle.penup()
# turtle.goto(50, 192)
# turtle.pendown()
# turtle.forward(20)
# turtle.right(90)
# turtle.forward(70)
# turtle.right(135)
# turtle.forward(30)
# turtle.right(45)
# turtle.forward(50)
# turtle.end_fill()
 
# # imzo
 
# turtle.penup()
# turtle.goto (-290, -150)
# turtle.color ("black")
# turtle.write("Uy tayyor ", font=("Arial", 18, "bold"))
# turtle.goto (-100, -190)
# turtle.color ("red")
# turtle.write("Tohirov Fatkhiddin", font=("Arial", 18, "bold"))
# turtle.done()
# turtle.screen.exitonclick()
# turtle.screen.mainloop()


from turtle import *

speed(5)

# Grass
bgcolor("green")

# Sky
penup()
goto(-400, -100)
pendown()
color("deepskyblue")
begin_fill()
for i in range(2):
    forward(800)
    left(90)
    forward(500)
    left(90)
end_fill()

# Sun
penup()
goto(-320, 225)
pendown()
color("yellow")
begin_fill()
circle(35)
end_fill()

# Cloud
penup()
goto(200, 200)
pendown()
color("white")
begin_fill()
circle(25)
end_fill()

penup()
goto(220, 240)
pendown()
begin_fill()
circle(25)
end_fill()

penup()
goto(230, 215)
pendown()
begin_fill()
circle(25)
end_fill()

penup()
goto(180, 225)
pendown()
begin_fill()
circle(25)
end_fill()

# House
penup()
goto(-100, -100)
pendown()
pensize(3)
color("chocolate", "orange") # (stroke, fill)
begin_fill()
for i in range(4):
    forward(170)
    left(90)
end_fill()

# Chimney
penup()
goto(20, 130)
pendown()
color("brown", "firebrick")
begin_fill()
for i in range(2):
    forward(40)
    left(90)
    forward(100)
    left(90)
end_fill()

# Roof
penup()
goto(-127, 70)
pendown()
begin_fill()
for i in range(3):
    forward(225)
    left(120)
end_fill()

# Window 1
penup()
goto(0, 0)
pendown()
color("black", "white")
begin_fill()
for i in range(4):
    forward(50)
    left(90)
end_fill()

# Window 1 Cross - Horizontal Line 
penup()
goto(0, 25)
pendown()
color("black")
forward(50)

# Window 1 Cross - Vertical Line 
penup()
goto(25, 0)
pendown()
left(90)
forward(50)

# Window 2
penup()
goto(-80, 0)
pendown()
right(90)
color("black", "white")
begin_fill()
for i in range(4):
    forward(50)
    left(90)
end_fill()

# Window 2 Cross - Horizontal Line 
penup()
goto(-80, 25)
pendown()
color("black")
forward(50)

# Window 2 Cross - Vertical Line 
penup()
goto(-55, 0)
pendown()
left(90)
forward(50)

# Door
penup()
goto(-40, -97)
pendown()
right(90)
color("red")
begin_fill()
for i in range(2):
    forward(50)
    left(90)
    forward(80)
    left(90)
end_fill()

# Door Handle
penup()
goto(-30, -60)
pendown()
color("black")
begin_fill()
circle(5)
end_fill()

hideturtle()
exitonclick()