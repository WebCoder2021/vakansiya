from django.contrib.auth.models import User
from import_export import resources, fields
from import_export.widgets import ForeignKeyWidget, ManyToManyWidget,DateWidget
from .models import Category,Type,HashTag,District,Comfort,Requirement,Vacancy

class VacancyDetailsAdminResource(resources.ModelResource):
    category = fields.Field(column_name='category', attribute='category', widget=ForeignKeyWidget(Category, field='name'))
    district = fields.Field(column_name='district', attribute='district', widget=ForeignKeyWidget(District, field='name'))
    type = fields.Field(column_name='type', attribute='type', widget=ForeignKeyWidget(Type, field='name'))
    comfort = fields.Field(column_name='comfort', attribute='comfort', widget=ManyToManyWidget(Comfort, field='name'))
    requirements = fields.Field(column_name='requirements', attribute='requirements', widget=ManyToManyWidget(model=Requirement,field='name'))
    hash_tag = fields.Field(column_name='hash_tag', attribute='hash_tag', widget=ManyToManyWidget(HashTag, field='name'))

    class Meta:
        model = Vacancy
        fields = (
            'id',
            'image',
            'title',
            'sub_title',
            'content',
            'address',
            'work_time',
            'salary',
            'phone',
            'phone2',
            'telegram',
            'comfort',
            'requirements',
            'link',
            'date',
            'publish',
            'post',
            'category',
            'type',
            'hash_tag',
            'district',
            'date_time'
        )