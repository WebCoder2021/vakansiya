from django.urls import path
from .views import *
urlpatterns = [
    path('', all_vacan,name='all_vacan'),
    path('vacan/<int:id>/detail/', vacan_detail,name='vacan_detail'),
    path('add_post/', add_post,name='add_post'),
    path('sended/', sended,name='sended'),
    path('added/', added,name='added'),
    path('add_vacan/', add_vacan,name='add_vacan'),
    path('edit_vacan/<int:id>/', edit_vacan,name='edit_vacan'),
]