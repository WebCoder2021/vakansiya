import os
from django.conf import settings
from django.shortcuts import redirect, render
import codecs
from app.send_backup import send_vacan
from django.db.models import Q
from .form import VacanForm
from django.contrib import messages
# from .send_backup import send_vacan
from .models import *
# Create your views here.
def all_vacan(request):
   context = {}
   context['category'] = Category.objects.all()
   context['type'] = Type.objects.all()
   context['district'] = District.objects.all()
   context['hash_tag'] = HashTag.objects.all()
   context['vacansy'] = Vacancy.objects.all().order_by('-id')
   if request.method == "GET":
      search = request.GET.get('search',False)
      if search:
         context['vacansy'] = Vacancy.objects.filter(Q(title__icontains=search)|Q(content__icontains=search)|Q(category__name__icontains=search)|Q(type__name__icontains=search)|Q(sub_title__icontains=search)|Q(address__icontains=search)|Q(work_time__icontains=search)|Q(salary__icontains=search)|Q(phone__icontains=search)|Q(phone2__icontains=search)|Q(telegram__icontains=search)|Q(type__name__icontains=search)|Q(district__name__icontains=search)|Q(rate__icontains=search)).order_by('-id')
         return render(request, 'vacan/all_vacan.html',context)
   if request.method == "POST":
      context['vacansy'] = Vacancy.objects.all().order_by('-id')
      post = request.POST
      district = post.get('district',False)
      type = post.get('type',False)
      category  = post.get('category',False)
      hash_tag = post.getlist('hash_tag',False)
      if district and district != "0":
         context['vacansy'] = context['vacansy'].filter(district_id=district).order_by('-id')
      if type and type != "0":
         context['vacansy'] = context['vacansy'].filter(type_id=type).order_by('-id')
      if category and category != "0":
         context['vacansy'] = context['vacansy'].filter(category_id=category).order_by('-id')
      if hash_tag and hash_tag != "0":
         context['vacansy'] = context['vacansy'].filter(hash_tag__in=hash_tag).order_by('-id')
   return render(request, 'vacan/all_vacan.html',context)
def vacan_detail(request,id):
   context = {}
   if Vacancy.objects.filter(id=id).exists():
      context['vacan'] = Vacancy.objects.filter(id=id).first()
      context['vacan_info'] = get_vacan_info(id)
      return render(request, 'vacan/vacan_detail.html',context)
   return render(request, 'vacan/index.html',context)

def sended(request):
    context = {}
    context['vacansy'] = Vacancy.objects.filter(post=True).all()
    return render(request, 'vacan/sended.html',context)
def added(request):
    context = {}
    context['vacansy'] = Vacancy.objects.filter(post=False,publish=False).all()
    return render(request, 'vacan/sended.html',context)
def add_post(request):
    context = {}
    context['vacansy'] = []
    for vacan in Vacancy.objects.filter(post=False,publish=True).all():
       context['vacansy'].append(get_vacan_info(vacan.id))
    if request.method == 'POST':
       post_id = request.POST.get('post_id',False)
       if post_id and Vacancy.objects.filter(id=post_id).exists():
         send_post(get_vacan_info(post_id))
         pt = Vacancy.objects.filter(id=post_id).first()
         pt.post = True
         pt.save()
         return render(request, 'add_post.html',context)
    return render(request, 'vacan/add_post.html',context)

def get_vacan_info(post_id):
   post_detail = {}
   if Vacancy.objects.filter(id=post_id).exists():
      vacan = Vacancy.objects.filter(id=post_id).first()
      data = ''
      data1 = ''
      if vacan.hash_tag:
         for ht in vacan.hash_tag.all():
            data+=f'%23{ht} '
         data+='\n\n'
      if vacan.title:
         data+=f'❗️<b>{vacan.title.upper()}</b>\n\n'
      if vacan.sub_title:
         data+=f'🍃{vacan.sub_title}\n\n'
      if vacan.content:
         data+=f'📎<i>{vacan.content}</i>\n\n'
      if vacan.comfort and len(vacan.comfort.all()) > 0:
         data+=f'✅<b>Qulayliklar:</b>\n'
         for cm in vacan.comfort.all():
            data+=f'-{cm}\n '
         data+='\n'
      if vacan.requirements and len(vacan.requirements.all()) > 0:
         data+=f'📌<b>Shartlar:</b>\n'
         for rq in vacan.requirements.all():
            data+=f'-{rq}\n '
         data+='\n\n'
      if vacan.address:
         data+=f'📍<b>Manzil: </b>'
         data+=f'{vacan.address}\n\n'
      if vacan.work_time:
         data+=f'⏰<b>Ish vaqti: </b> '
         data+=f'{vacan.work_time}\n\n'
      if vacan.salary:
         data+=f'💰<b>Maosh: </b>'
         data+=f'{vacan.salary}\n\n'
      if vacan.rate:
         data+=f'🗞<b>Ish stavkasi: </b>'
         data+=f'{vacan.rate}\n\n'
      if vacan.phone or  vacan.telegram or vacan.link:
         data+=f'<b>Murojaat uchun:</b>\n'
         if vacan.phone:
            data+=f'📞<b><i>Telefon raqam: </i></b>'
            data+=f'{vacan.phone}  '
         if vacan.phone2:
            data+=f'{vacan.phone2}\n'
         if vacan.telegram:
            data+=f'📱<b><i>Telegram: </i></b>'
            data+=f'{vacan.telegram}\n'
         if vacan.link:
            data+=f'🔗<b><i>URL: </i></b>'
            data+=f'{vacan.link}\n'
         data+=f'\n\n'
      data+='<a href="http://cace.buxdu.uz/">Karyera Markazi</a> | <a href="https://t.me/Buxdumarketing">Telegram</a>'
      if vacan.image:
          url = fr'{os.path.dirname(os.path.realpath(__file__))}{vacan.image.url}'
          url = url.replace('\\', '/').replace('app/', '')
          post_detail['url'] = url
   post_detail['id'] = post_id
   if vacan.image:
      post_detail['image'] = vacan.image.url
   post_detail['data'] = data
   post_detail['data1'] = data.replace('\n', '<br>').replace('%23','#')
   return post_detail
def send_post(post_detail):
   if post_detail['url']:
      file = {'photo':open(post_detail['url'],"rb")}
      send_vacan(post_detail['data'],file)
   else:
      send_vacan(post_detail)



def add_vacan(request):
   context = {}
   context['category'] = Category.objects.all()
   context['type'] = Type.objects.all()
   context['district'] = District.objects.all()
   context['hash_tag'] = HashTag.objects.all()
   if request.method == "POST":
      post = request.POST
      image = request.FILES.get('image',False)
      title = post.get('title',False)
      sub_title = post.get('sub_title',False)
      content = post.get('content',False)
      address = post.get('address',False)
      work_time = post.get('work_time',False)
      salary = post.get('salary',False)
      phone = post.get('phone',False)
      phone2 = post.get('phone2',False)
      comfort = post.get('comfort',False)
      requirements = post.get('requirements',False)
      link = post.get('link',False)
      category = post.get('category',False)
      type = post.get('type',False)
      hash_tag = post.getlist('hash_tag',False)
      district = post.get('district',False)
      rate = post.get('rate',False)
      vacan = Vacancy.objects.create(title=title,category_id=category,type_id=type,district_id=district,user=request.user)
      if image:
         vacan.image = image
      if sub_title:
         vacan.sub_title = sub_title
      if content:
         vacan.content = content
      if address:
         vacan.address = address
      if work_time:
         vacan.work_time = work_time
      if salary:
         vacan.salary = salary
      if phone:
         vacan.phone = phone
      if phone2:
         vacan.phone2 = phone2
      if link:
         vacan.link = link
      if rate:
         vacan.rate = rate
      if comfort and len(comfort) > 0:
         comforts = comfort.split('\n')
         for cm in comforts:
            com = Comfort.objects.create(name=cm)
            com.save()
            vacan.comfort.add(com)
      if requirements and len(requirements) > 0:
         rqs = requirements.split('\n')
         for cm in rqs:
            com = Requirement.objects.create(name=cm)
            com.save()
            vacan.requirements.add(com)
      for hts in hash_tag:
         ht = HashTag.objects.filter(id=hts).first()
         vacan.hash_tag.add(ht)
      vacan.save()

   return render(request,"vacan/add_vacan.html", context)

def edit_vacan(request,id):
   context = {}
   context['category'] = Category.objects.all()
   context['type'] = Type.objects.all()
   context['district'] = District.objects.all()
   context['hash_tag'] = HashTag.objects.all()
   if Vacancy.objects.filter(id=id).exists():
      vacan = Vacancy.objects.filter(id=id).first()
      context['vacan'] = vacan
      if request.method == "POST":
         post = request.POST
         image = request.FILES.get('image',False)
         title = post.get('title',False)
         sub_title = post.get('sub_title',False)
         telegram = post.get('telegram',False)
         content = post.get('content',False)
         address = post.get('address',False)
         work_time = post.get('work_time',False)
         salary = post.get('salary',False)
         phone = post.get('phone',False)
         phone2 = post.get('phone2',False)
         comfort = post.get('comfort',False)
         requirements = post.get('requirements',False)
         link = post.get('link',False)
         category = post.get('category',False)
         types = post.get('type',False)
         hash_tag = post.getlist('hash_tag',False)
         district = post.get('district',False)
         rate = post.get('rate',False)
         if image and vacan.image != image:
            print("image",image)
            vacan.image = image
         if category and vacan.category.id != int(category):
            print(type(vacan.category.id))
            print("category",category)
            vacan.category.id = category
         if types and vacan.type.id != int(types):
            print(vacan.type,"type",types)
            vacan.type.id = types
         if district and vacan.district.id != int(district):
            print("district",district)
            vacan.district.id = district
         if title and vacan.title != title:
            print("title",title)
            vacan.sub_title = sub_title
         if sub_title and vacan.sub_title != sub_title:
            print("sub_title",sub_title)
            vacan.sub_title = sub_title
         if telegram and vacan.telegram != telegram:
            print("telegram",telegram)
            vacan.telegram = telegram
         if content and vacan.content != content:
            print("content",content)
            vacan.content = content
         if address and vacan.address != address:
            print("address",address)
            vacan.address = address
         if work_time and vacan.work_time != work_time:
            print("work_time",work_time)
            vacan.work_time = work_time
         if salary and vacan.salary != salary:
            print("salary",salary)
            vacan.salary = salary
         if phone and vacan.phone != phone:
            print("phone",phone)
            vacan.phone = phone
         if phone2 and vacan.phone2 != phone2:
            print("phone2",phone2)
            vacan.phone2 = phone2
         if link and vacan.link != link:
            print("link",link)
            vacan.link = link
         if rate and vacan.rate != rate:
            print("rate",rate)
            vacan.rate = rate
         # if comfort and len(comfort) > 0:
         #    print('comfort',comfort)
         #    comforts = comfort.split('\n')
         #    for cm in comforts:
         #       com = Comfort.objects.create(name=cm)
         #       com.save()
         #       vacan.comfort.add(com)
         # if requirements and len(requirements) > 0:
         #    rqs = requirements.split('\n')
         #    for cm in rqs:
         #       com = Requirement.objects.create(name=cm)
         #       com.save()
         #       vacan.requirements.add(com)
         # for hts in hash_tag:
         #    print('hash tag',hash_tag)
         #    ht = HashTag.objects.filter(id=hts).first()
            # vacan.hash_tag.add(ht)
            
         vacan.save()
         context['vacan'] = vacan
         return render(request,"vacan/edit_vacan.html", context)

   return render(request,"vacan/edit_vacan.html", context)