from django.http import JsonResponse
from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from users.models import CustomUser
from .captcha import FormWithCaptcha
# Create your views here.
def log_in(request):
    context = {}
    context['captcha'] = FormWithCaptcha()
    if request.method == "POST":
        form = FormWithCaptcha(request.POST)
        if form.is_valid():
            post = request.POST
            phone = post.get('phone', False)
            password = post.get('password', False)
            user = post.get('user', False)
            if user:
                user = CustomUser.objects.filter(id=user).first()
            else:
                password2 = post.get('password2', False)
                if password == password2:
                    first_name = post.get('first_name', False)
                    last_name = post.get('last_name', False)
                    middle_name = post.get('middle_name', False)
                    email = post.get('email', False)
                    location = post.get('location', False)
                    company_name = post.get('company_name', False)
                    position = post.get('position', False)
                    if first_name and last_name and middle_name and location and company_name and position:
                        user = CustomUser.objects.create(
                            phone=phone, first_name=first_name, last_name=last_name, middle_name=middle_name,company_name=company_name,position=position,location=location)
                        if email:
                            user.email = email
                        user.set_password(password)
                        user.save()
                    else: context['error'] = "Ma'lumotlar to'liq kiritilmadi"
                else:
                    context['error'] = "Qayta kiritilgan parol xato"
                    return render(request, 'users/login.html', context)
            sign_in = authenticate(request, phone=phone, password=password)
            if sign_in is not None:
                            login(request,user)
                            return redirect('all_vacan')
            else: context['error'] = 'Login yoki parol xato'
        else: context['error'] = 'Iltimos robot emasligingizni isbotlang'
    else: user = None
    return render(request, 'users/login.html',context)
    
def logout_view(request):
    msg =  request.GET.get('logout',False)
    if msg and msg == "true":
        logout(request)
        return redirect("all_vacan")
    return render(request, 'users/logout_view.html')
def check_user(request):
    phone=request.GET.get('phone')
    user = CustomUser.objects.filter(phone=phone).first()
    if user is not None:
        return JsonResponse({'user':user.id})
    return JsonResponse({'user':False})